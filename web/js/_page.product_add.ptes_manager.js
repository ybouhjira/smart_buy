/**
  * _page.product_add.ptes_manager.js
  * This script is for adding the properties to the table in 
  * the product adding form 
  */

$(function(){
    /**
      * This will contain the properties name and values 
      * fetched by Ajax
      */
    var properties = [];

    (function main(){
        // fetch the properties using Ajax
        fetchData();
        // event handlers
        $('#add-pte-button').click(function(e){
            e.preventDefault();
            addProperty();
        });
    })();

    /**
      * fetches data from the server and puts it in properties var
      */
    function fetchData(){
        $.getJSON(
            '_json.product_add.fetch_properties.php',
            {},
            function(data){
                properties = data;
                // adding a first property
                addProperty();
            }
        )
    }

    /**
      * The add button click event handler
      * adds a new row to the table
      */
    function addProperty(){
        // Appending a line to the table 
        $("#pte-table").trAppend(
            $('<tr>').append(
                // column 1 : Name of the property ----------------
                $('<td>').append( 
                    $('<select>',{
                        name:'properties[]',
                        required: 'required'
                    }).append(
                        $.map(properties,function(p){
                            return $('<option>')
                                .attr('data-unite',p.unite)
                                .html(p.nom_propriete) ;
                        })
                    ).change(setPropertyValueInput)
                ),
                // Column 2 : value of the property -----------------
                $('<td>'),
                // Column 3 : Contains the delete button -------------
                $('<td>').append( 
                    $('<a>').addClass('btn btn-danger').append(
                        $('<i>').addClass('icon-trash')
                    ).click(removeProperty)
                )
            )
        ); //<--- END OF $("#pte-table").append()
        
        // Set the correct input type for the property
        // <input type="number"> or <textarea>
        var curSelect = $("#pte-table tr:last-child td:first-child select");
        setPropertyValueInput.call(curSelect);
    }//<--- END OF FUNCTION addProperty

    /**
      * Event Listener for the delete button
      */
    function removeProperty(){
        if($('#pte-table tr select').length > 1)
            $(this).closest('tr').trRemove(); 
        else
            $('#single-pte-delete').modal();
    }

    /**
      * This is the event handler that changes the value input according to
      * the name of the property
      * 
      * this function should be called using .call() to change it's context
      * to the desired <select> element
      */
    function setPropertyValueInput(){
        // the unit of the property
        var unit = $(this).find('option:selected').attr('data-unite');
        
        // the <td> for second column
        var td2 = $(this).parent('td').next('td').empty() ;

        // Append the input
        if(unit){ //<----- numerical property : <input type='number'>
            td2.append(
                $('<div>').addClass('input-append').append(
                    $('<input>',{
                        type: 'text',
                        name: 'ptevalues[]',
                        required: 'required'
                    }),
                    $('<span>').addClass('add-on').html(unit)
                ),
                $('<input>',{
                        type: 'hidden',
                        name: 'ptetypes[]',
                        value: 'num'
                    }
                )
            )
        }else{ //<---- text property : <textarea>
            td2.append(
                $('<textarea>',{
                        name:'ptevalues[]',
                        required: 'required'
                    }
                ),
                $('<input>',{
                        type: 'hidden',
                        name: 'ptetypes[]',
                        value: 'text'
                    }
                ) 
            )
        }

        // check for duplicates
        var theOption = $(this).find('option:selected') ;
        theOption.closest('tr').removeClass('error');
        $('#pte-table option:selected').each(function(){
            if( $(this).html() == theOption.html() && this != theOption[0] ){
                theOption.closest('tr').addClass('error')
            }
        })
    }//<--- END OF FUNCTION setPropertyValueInput
});
