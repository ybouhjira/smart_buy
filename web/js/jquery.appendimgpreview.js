(function($){
    $.fn.appendImgPreview = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var elem = this;
            reader.onload = function (e) {
                $(elem).append(
                    $('<img>',{
                        src : e.target.result,
                        class: 'img-polaroid'
                    })
                ).hide().stop().fadeIn('slow') //<-- animation
            }
            reader.readAsDataURL(input.files[0])
        }
    }
})(jQuery)