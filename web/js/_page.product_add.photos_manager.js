/**
  * _page.product_add.photo_manager.js
  * This script is for adding the photos to the table in 
  * the product adding form
  */

$(function(){

    (function main(){
        addPhotoInput();
    })();
    /**
      * Add button click handler
      */
    $("#btn-add-photo").click(function(e){
        e.preventDefault();
        addPhotoInput();
    })

    /**
     * Adds a <tr>
     */
    function addPhotoInput(){
        // The <tr> to be appended
        $('#photos-table').trAppend(
            $('<tr>').append(
                $('<td>').append(
                    $('<input>',
                        {
                            type: 'file',
                            name: 'photos[]',
                            required: 'required'
                        }
                    ).change(setPhotoPreview)
                ),
                $('<td>'),
                $('<td>').append(
                    $('<a>',
                        {
                            href: '#',
                            class: 'btn btn-danger '
                        }
                    ).click(removePhotoInput).append(
                        $('<i>').addClass('icon-trash')
                    )
                )
            )
        )
    }

    /**
     * deletes a <tr>
     */
    function removePhotoInput(e){
        e.preventDefault();
        if($('#photos-table input[type="file"]').length > 1 )
            $(this).closest('tr').trRemove();
        else
            $('#single-photo-delete').modal();
    }

    /**
      * Change the preview image
      */
      function setPhotoPreview(){
        // this = <input type='file'>
        $(this)
            .closest('tr')
            .find('td:nth-child(2)')
            .empty()
            .appendImgPreview(this);
      }
});
