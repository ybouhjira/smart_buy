(function($){
    $.fn.trRemove = function(){
        $(this).stop().hide('normal', function(){
            $(this).removeClass('error').remove();
        }).addClass('error');
    }
})(jQuery)