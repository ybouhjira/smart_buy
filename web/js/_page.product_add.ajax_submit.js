/**
  * _page.product_add.ajax_submit.js
  * This script configures the jQuery Form plugin 
  * to submit the form using Ajax
  */

$(function(){
    $("form").ajaxForm({
        dataType: 'ajax',
        beforeSubmit : function(){
            $('#submit-dialog').modal();
            $('input[type="submit"]')
                .attr('disabled','disabled')
                .removeClass('btn-primary')
        },
        uploadProgress: function(e, pos, total, percent){
            $('#submit-dialog .progress .bar').css('width',percent+'%');
        },
        error: function(a,b,c){
            // Display the model
            $('#submit-dialog').modal();
            // Display the error message
            $('<div>')
                .addClass('alert alert-error')
                .html('Erreur interne dans le serveur!<br>'+a+'<br>'+b+'<br>'+c)
                .insertBefore('#submit-dialog .progress');
            // change progress bar apearance
            $('#submit-dialog .progress')
                .addClass('progress-danger')
                .removeClass('active progress-striped');
            // enable the submit button
            $('input[type="submit"]')
                .removeAttr('disabled','disabled')
                .addClass('btn-alert')
        },
        success: function(resp){
            console.log(resp);
            switch(resp.status){
                case 'success':
                    $('#submit-dialog').modal();
                    $('<div>')
                        .addClass('alert alert-success')
                        .html(resp.message)
                        .insertBefore('#submit-dialog .progress');
                    $('#submit-dialog .progress')
                        .addClass('progress-success')
                        .removeClass('active progress-striped');
                    break;
                default:
                    // Display the model
                    $('#submit-dialog').modal();
                    // Display the error message
                    $('<div>')
                        .addClass('alert alert-error')
                        .html(resp.message)
                        .insertBefore('#submit-dialog .progress');
                    // change progress bar apearance
                    $('#submit-dialog .progress')
                        .addClass('progress-danger')
                        .removeClass('active progress-striped');
                    // enable the submit button
                    $('input[type="submit"]')
                        .removeAttr('disabled','disabled')
                        .addClass('btn-alert')
            }
        }
    })
});