(function($){
    $.fn.trAppend = function(tr){
        $(this).append(tr)
        tr.hide().stop().show('slow', function(){
            tr.removeClass('success');
        }).addClass('success');
    }
})(jQuery)