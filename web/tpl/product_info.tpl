{extends file="$SITEDIR/web/tpl/panel.tpl"}

{block name=title}{$prod->nom_produit}{/block}
{block name=content}
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active">
            <a href="#product-tab" data-toggle="tab">
                <i class="icon-shopping-cart"></i>
                Produit
            </a>
        </li>
        <li>
            <a href="#ptes-tab" data-toggle="tab">
                <i class="icon-info-sign"></i>
                Propri&eacute;t&eacute;s
            </a>
        </li>
        <li>
            <a href="#photos-tab" data-toggle="tab">
                <i class="icon-th-large"></i>
                Photos
            </a>
        </li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        {* PRODUIT ---------------------------------------------------------- *}
        <div class="tab-pane active" id="product-tab">
            <ul>
                <li>
                    <strong>Nom: </strong> 
                    {$prod->nom_produit}
                </li>
                <li>
                    <strong>Cat&eacute;gorie: </strong> 
                    {$prod->id_categorie}
                </li>
                <li>
                    <strong>Prix : </strong>
                    {$prod->prix} DH
                </li>
                <li>
                    <strong>Marque : </strong>
                    {$prod->marque}
                </li>
                <li>
                    <strong> TVA :  </strong>
                    {$prod->tva * 100} %
                </li>
                <li>
                    <strong>Description : </strong>
                    <p>{$prod->description}</p>
                </li>
            </ul>
        </div>
        {* PROPRIETES ------------------------------------------------------- *}
        <div class="tab-pane" id="ptes-tab">
            <table class="table table-bordered">
                <tr>
                    <td>Propri&eacute;t&eacute;</td>
                    <td>Valeur</td>
                </tr>
                {foreach $ptes as $pt}
                    <tr>
                        <td>{$pt->nom_pte}</td>
                        <td>{$pt->value}</td>
                    </tr>
                {/foreach}
            </table>
            <form 
                class="form-horizontal well" 
                action="controlers/property_add.php"
                method="post" 
                >
                <input type="hidden" value="{$prod->id_produit}" name="id"/>
                <fieldset>
                    <legend>Ajouter une propriété</legend>
                    <div class="control-group">
                        <label class="control-label" >Nom</label>
                        <div class="controls">
                            <input type="text" name="name" required/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Valeur</label>
                        <div class="controls">
                            <textarea name="value" required></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-plus"></i> Ajouter
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        {* PHOTOS ------------------------------------------------------------*}
        <div class="tab-pane" id="photos-tab">
            {foreach $photos as $ph}
                <img src='{$ph->fichier_photo}' alt="" />
            {/foreach}
            <form
                action="controlers/photo_add.php"
                method="post"  
                class="form-inline well"
                enctype="multipart/form-data"
            >
                <input type="hidden" name="id" value="{$prod->id_produit}" />
                <label>Fichier : </label>
                <input type="file" name="photo" required="required" />
                <button type="submit" class="btn btn-success">
                    <i class="icon-plus"></i> Ajouter
                </button>
            </form>
        </div>
    </div>
{/block}    
