{extends file="web/tpl/base.tpl"}

{block name=title}
  SmartBuy - Le meilleur site e-commerce pour equipement informatique
{/block}

{block name=stylesheets}
<link href="/{$SUBDIR}web/css/layout.css" rel="stylesheet" />
{/block}

{block name=body}
{* NAVBAR --------------------------------------------------------------------*}
<div class="container navbar-wrapper">
  <div class="navbar navbar-inverse">
    <div class="navbar-inner">
      <a  class="btn btn-navbar" data-toggle="collapse" 
      data-target=".nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <a class="brand" href="#">SmartBuy</a>
    <div class="nav-collapse collapse">
      <ul class="nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#contact">Contact</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Dropdown 
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li class="nav-header">Nav header</li>
            <li><a href="#">Separated link</a></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>
</div>

{* SLIDES ------------------------------------------------------------------- *}
<div id="myCarousel" class="carousel slide">
  <div class="carousel-inner">
    <div class="item active">
      <img src="../assets/img/examples/slide-01.jpg" alt="">
      <div class="container">
        <div class="carousel-caption">
          <h1>Example headline.</h1>
          <p class="lead">Cras justo odio, dapibus ac facilisis in, 
            egestas eget quam. Donec id elit non mi porta gravida at eget 
            metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          <a class="btn btn-large btn-primary" href="#">Sign up today</a>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="../assets/img/examples/slide-02.jpg" alt="">
      <div class="container">
        <div class="carousel-caption">
          <h1>Another example headline.</h1>
          <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas
           eget quam. Donec id elit non mi porta gravida at eget metus. 
           Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          <a class="btn btn-large btn-primary" href="#">Learn more</a>
        </div>
      </div>
    </div>
    <div class="item">
      <img src="../assets/img/examples/slide-03.jpg" alt="">
      <div class="container">
        <div class="carousel-caption">
          <h1>One more for good measure.</h1>
          <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas
           eget quam. Donec id elit non mi porta gravida at eget metus. 
           Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
          <a class="btn btn-large btn-primary" href="#">Browse gallery</a>
        </div>
      </div>
    </div>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    &lsaquo;
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    &rsaquo;
  </a>
</div>
{/block}