{extends file="$SITEDIR/web/tpl/panel.tpl"}

{block name=title}Ajouter un produit{/block}

{block name=stylesheets append}
<link rel="stylesheet" type="text/css" href="/{$SUBDIR}web/css/client_add.css"/>
{/block}

{block name=content}
<h2>Ajouter un produit</h2>
<form
    action="controlers/product_add.php" 
    class="form form-horizontal frontend-validated" 
    method="post" >
{* INOFORMATION SUR LE PRODUIT ---------------------------------------------- *}
    <fieldset>
        {* CATEGORIE *}
        <legend>Informations sur le produit</legend>
        <div class="control-group">
            <label class="control-label"> Cat&eacute;gorie : </label>
            <div class="controls">
              <select name="categorie" required="required">
                {foreach $categories as $option}
                    <option value="{$option->id_categorie}">
                        {$option->nom_categorie}
                    </option>
                {/foreach}
              </select>
            </div>
        </div>
        {* NOM *}
        <div class="control-group">
            <label class="control-label"> Nom : </label>
            <div class="controls">
                <input type="text" name="nom_produit" required="required" />
            </div>
        </div>
        {* PRIX *}
        <div class="control-group">
            <label class="control-label"> Prix : </label>
            <div class="controls">
                <input type="text" name="prix" required="required" />
            </div>
        </div>
        {* MARQUE *}
        <div class="control-group">
            <label class="control-label"> Marque : </label>
            <div class="controls">
                <input type="text" name="marque" required="required" />
            </div>
        </div>
        {*  TVA *}
        <div class="control-group">
            <label class="control-label"> TVA : </label>
            <div class="controls">
                <input type="text" name="tva" required="required" />
            </div>
        </div>
        {*  DESCIRPTION *}
        <div class="control-group">
            <label class="control-label"> Descirption : </label>
            <div class="controls">
                <textarea name="description" required="required" ></textarea>
            </div>
        </div>
    </fieldset>

    
{* SUBMIT  -------------------------------------------------------------- *}
    <div class="control-group">
        <div class="controls">
            <input type="submit" value="Valider" 
                class="btn btn-primary btn-large"/>
        </div>
    </div>
</form>

{/block}