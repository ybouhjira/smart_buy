{extends file="$SITEDIR/web/tpl/base.tpl"}

{block name=title}
  Bonjour Administrateur
{/block}

{block name=stylesheets}
<link href="/{$SUBDIR}web/css/admin_index.css" rel="stylesheet" />
{/block}

{block name=body}
<table>
    <tr>
        <td>
            <div class="container-fluid form-wrapper">
                <h1>Bienvenue</h1>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">
                        &times;
                    </button>
                    <strong>Erreur!</strong> E-mail ou mot de passe erronés
                </div>
                <form >
                    <div class="row-fluid">
                        <div class="input-prepend span12">
                            <span class="add-on">
                                <i class="icon-envelope"></i>
                            </span>
                            <input type="email" name="email" 
                                placeholder="E-mail"/>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-prepend span12">
                            <span class="add-on">
                                <i class="icon-lock"></i>
                            </span>
                            <input type="password" name="pass" 
                                placeholder="Mot de passe"/>
                        </div>
                    </div>
                    <div id="submit-container">
                        <input type="submit" value="Se connecter" 
                            class="btn btn-primary"/>
                    </div>
                </form>
            </div>
        </td>
    </tr>
</table>
{/block}