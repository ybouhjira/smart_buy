{extends file="$SITEDIR/web/tpl/panel.tpl"}

{block name=title}Donn&eacute;es invalides{/block}

{block name=content}
{foreach $messages as $msg}
<div class="alert alert-error">
    {$msg}
</div>
{/foreach}
<div class="centering">
    <a href="../product_add.php" class="btn btn-primary btn-large">
        <i class="icon-refresh icon-white"></i> R&eacute;ssayer
    </a>
</div>
{/block}