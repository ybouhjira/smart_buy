<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{block name=title}{/block}</title>
    {* STYLESHEETS ---------------------------------------------------------- *}
    <link href="/{$SUBDIR}web/bootstrap/css/bootstrap.min.css" 
      rel="stylesheet"/>
    <link href="/{$SUBDIR}web/bootstrap/css/bootstrap-responsive.css" 
      rel="stylesheet"/>
    {block name=stylesheets}{/block}
    <script src="/{$SUBDIR}web/js/jquery-1.8.3.min.js"></script>
    <script src="/{$SUBDIR}web/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    {block name=body}{/block}
    {block name=javascript}{/block}
</body>
</html>