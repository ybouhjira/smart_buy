<?php
/**
  * _json.product_add.fetch_properties.php
  * this file responds to the ajax query from _page.product_add.ptes_manager.js
  */

require_once '../lib/config.php';
$db = new Database();
$ptesN = $db->fetchObjects('SELECT * FROM ProprietesNum ');
$ptesAN = $db->fetchObjects('SELECT * FROM ProprietesAlphaNum'); 

header('Content-type: application/json');
echo json_encode(array_merge($ptesAN,$ptesN));
?>