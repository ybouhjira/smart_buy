<?php
require_once '../../lib/config.php';

$prodException = new HttpGetException("Erreur! Produit invalide") ;

try {
    // CONNECT TO DATABASE -----------------------------------------------------
    $db = new Database();

    // check if the product exists
    $prodId = $db->quote($_POST['id']) ;
    $prodStm = $db->query("SELECT 1 FROM Produits WHERE id_produit = $prodId");
    if(!($prodStm->rowCount()>0))
        throw $prodException ;

    // check the file
    $form = new Form();
    $mimetype = '/^image\/(png|jpeg|jpg|bmp)$/' ;
    $form->addInput(new FileIn(500*1024,$mimetype,'photo'));
    $form->validate();

    // insert the file into the database
    $fileData = file_get_contents($_FILES['photo']['tmp_name']);
    $db->insert('PhotoProduit',array(
            'id_produit' => $_POST['id'],
            'fichier_photo' => $fileData
        )
    );

    // SEND THE USER TO THE PRODUCT'S PAGE -------------------------------------
    header('Location: ../product_info.php?id='.$_POST['id']);

} catch (PDOException $e) {
    $pageError = new AdminErrorPage();
    $pageError->display();
} catch(HttpGetException $exc) {
    $errorPage = new AdminErrorPage($exc->getMessage());
    $errorPage->display();
} catch(FormException $exc) {
    $msg = 'Le fichier depasse la tailler de 500ko, ou est de format invalide' ;
    $errorPage = new AdminErrorPage($msg);
    $errorPage->display();
}

?>