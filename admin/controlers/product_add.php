<?php
/**
  * admin/controlers/product_add.php
  */ 

try{
    require_once '../../lib/config.php';

    // VALIDATING THE USER INPUT -----------------------------------------------
    // Fetch categories id
    $db = new Database();
    $ids = $db->fetchCol('id_categorie','Categorie');
    $catgPreg = '/^('.implode('|',$ids).')$/';

    // Validate product information
    $form = new Form('post');
    $form->addInput(array(
        new PregIn ($catgPreg, 'categorie'),
        new PregIn (PregIn::exp('ALNUMSPC',256,3), 'nom_produit'),
        new NumIn (0, INF,'prix'),
        new PregIn ('/[a-z]+.{0,44}/i', 'marque'),
        new NumIn (0, 1, 'tva'),
        new PregIn ('/^([a-z].*?){20,}$/si', 'description')
    ));
    $form->validate(false);

    // INSERTING DATA INTO THE DATABASE ----------------------------------------
    // insert product
    $db->insert('Produits',array(
            'prix' => $_POST['prix'],
            'marque' => $_POST['marque'],
            'nom_produit' => $_POST['nom_produit'],
            'tva' => $_POST['tva'],
            'id_categorie' => $_POST['categorie'],
            'description' => $_POST['description']
        )
    );
    $productId = $db->lastInsertId() ;

    // SEND THE USER TO THE PRODUCT PAGE ---------------------------------------
    header('Location: ../product_info.php?id='.$productId);

// EXCEPTIONS HANDLING ---------------------------------------------------------
}catch(PDOException $exp){
    $errorPage = new AdminErrorPage();
    $errorPage->display();
}catch(FormException $exp){
    $errors = array();
    foreach ($exp->getMessages() as $msg) {
        switch ($msg) {
            case 'categorie':
                $errors[] = 'Catégorie invalide';
                break;
            case 'nom_produit':
                $errors[] = 'Nom invalide';
                break;
            case 'prix':
                $errors[] = 'Prix invalide';
                break;
            case 'marque':
                $errors[] = 'Marque invalide';
                break;
            case 'tva':
                $errors[] = 'La TVA doit &ecirc;tre compris entre 0 et 1 ';
                break;
            case 'description':
                $errors[] = 'La description doit contenir au moins 20 lettres';
                break;
        }
    }
    // Display the error page with Smarty
    $smarty = new SmartySetup();
    $smarty->assign('messages',$errors);
    $smarty->display(SITEDIR.'web/tpl/form_error_page.tpl');
}
?>