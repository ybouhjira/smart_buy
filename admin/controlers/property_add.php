<?php
require_once '../../lib/config.php';

$prodException = new HttpGetException("Erreur! Produit invalide") ;

try {
    // CHECK GET PARAMETERS ----------------------------------------------------
    if(empty($_POST['name']) || empty($_POST['value']) || empty($_POST['id']))
        throw $prodException;

    // CONNECT TO DATABASE -----------------------------------------------------
    $db = new Database();
    
    // check if the product exists
    $prodId = $db->quote($_POST['id']) ;
    $prodStm = $db->query("SELECT 1 FROM Produits WHERE id_produit = $prodId");
    if(!($prodStm->rowCount()>0))
        throw $prodException ;

    // check if property existes
    $name = $db->quote($_POST['name']);
    $query = "SELECT id_pte FROM Propriete WHERE nom_pte = $name";
    $statement = $db->query($query);
    $idPte;
    if($statement->rowCount() <= 0){
        $db->insert('Propriete',array('nom_pte' => $_POST['name']) );
        $idPte = $db->lastInsertId();
    }else{
        $row = $statement->fetch();
        $idPte = $row['id_pte'] ;
    }

    // insert the value 
    $db->insert('ProduitPtes',array(
            'id_produit' => $_POST['id'],
            'id_pte' => $idPte,
            'value' => $_POST['value']
        )
    );

    // SEND THE USER TO THE PROPERTIES SECTION IN THIS PRODUCT'S PAGE ----------
    header('Location: ../product_info.php?id='.$_POST['id'].'#ptes-tab');


    
} catch (PDOException $e) {
    $pageError = new AdminErrorPage();
    $pageError->display();
} catch(HttpGetException $exc) {
    $errorPage = new AdminErrorPage($exc->getMessage());
    $errorPage->display();
}

?>