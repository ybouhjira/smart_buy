<?php
try{
    require_once '../lib/config.php';
    
    // Querying database
    $db = new Database();
    $categories = $db->fetchObjects('SELECT * FROM Categorie');
    $marques = $db->fetchObjects('SELECT DISTINCT marque FROM Produits');
    
        
    // Displaying the view with smarty
    $smarty = new SmartySetup();
    $smarty->assign(array(
        'categories' => $categories,
        'marques' => $marques
    ));
    $smarty->display('../web/tpl/product_add.tpl');

}catch(PDOException $e){
    $errorPage = new AdminErrorPage();
    $error->display();
}
?>