<?php
require_once '../lib/config.php';
$invalidProduct = new HttpGetException('Produit inexistant');
try{
    // CHECK THE PRODUCT ID ----------------------------------------------------
    if(empty($_GET['id']))
        throw $invalidProduct ;

    // FETCH DATA FROM DATABASE ------------------------------------------------
    $db = new Database();

    // Fetch product 
    $query = "SELECT * FROM Produits WHERE id_produit = {$_GET['id']}" ;
    $prod = $db->fetchObjects($query);
    if(!$prod)
        throw $invalidProduct;
    else
        $prod = $prod[0];

    // Fetch properties
    $quotedIdProd = $db->quote($_GET['id']);
    $query = 
        'SELECT pt.nom_pte, pp.value '.
        'FROM ProduitPtes pp JOIN Propriete pt '.
        'ON pp.id_pte = pt.id_pte '.
        "WHERE pp.id_produit = $quotedIdProd";
    $ptes = $db->fetchObjects($query);

    //fetch photos
    $query = 
        "SELECT fichier_photo FROM PhotoProduit ".
        "WHERE id_produit = $quotedIdProd" ;
    $photos = $db->fetchObjects($query);

    // DISPLAY THE PAGE --------------------------------------------------------
    $smarty = new SmartySetup();
    $smarty->assign(array(
            'prod' => $prod,
            'ptes' => $ptes,
            'photos' => $photos
        )
    );
    $smarty->display(SITEDIR.'web/tpl/product_info.tpl');

}catch(PDOException $exc){
    $errorPage = new AdminErrorPage();
    $errorPage->display();
}catch(HttpGetException $exc){
    $errorPage = new AdminErrorPage($exc->getMessage());
    $errorPage->display();
}
?>